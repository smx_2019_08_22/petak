/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.sessionBeans;

import entities.Ocene;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Grupa1
 */
@Local
public interface OceneSessionBeanLocal {
    public List<Ocene> getOceneUcenika(Integer ucenik);
}
