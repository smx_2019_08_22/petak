package business.sessionBeans;

import entities.Odeljenja;
import entities.Ucenici;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class StudentSessionBean implements StudentSessionBeanLocal {
    
    @PersistenceContext(unitName="SMSPU")
    private EntityManager em;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public List<Ucenici> getAllStudents() {
        
        try {
        Query query = em.createNamedQuery("Ucenici.findAll");
        List <Ucenici> ucenici = (List<Ucenici>) query.getResultList();
        return ucenici;
        }
        catch (NoResultException nre) {
            return null;
        } 
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    } 
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
        public List<Ucenici> getAllStudentsFromDepartment(Integer odeljenje) {
        
        try {
        Query query = em.createNamedQuery("Ucenici.findUceniciByOdeljenje");
        Query pronadjiOdeljenje = em.createNamedQuery("Odeljenja.findById");
        pronadjiOdeljenje.setParameter("id", odeljenje);
        Odeljenja odeljenja = (Odeljenja) pronadjiOdeljenje.getSingleResult();
        query.setParameter("odeljenja", odeljenja);
        List <Ucenici> ucenici = (List<Ucenici>) query.getResultList();
        return ucenici;
        }
        catch (NoResultException nre) {
            return null;
        } 
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
